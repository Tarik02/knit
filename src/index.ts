// ¯\_(ツ)_/¯
import './env';
import Telegraf, { Markup } from 'telegraf';
import moment from 'moment-timezone';
import { createServer } from 'http';

import { c, schedule, getPart, week } from './config';

if (!process.env.TOKEN)
	throw new Error('Token is required to start');

moment.tz.setDefault('Europe/Kiev');
const bot = new Telegraf(process.env.TOKEN);

bot.catch(console.error);

bot.start(async (ctx) => {
	const m = new Markup();
	await ctx.replyWithMarkdown('*λ* Кнопошкі 👇',
		m.keyboard([
			['ІПЗ', 'КСМ (КІ)']
		])
			.oneTime(false)
			.resize()
			.extra({ parse_mode: 'Markdown' })
	);
});

bot.hears('ІПЗ', async (ctx) => {
	const m = new Markup();
	await ctx.replyWithMarkdown('*λ* Вибирай 👇',
		m.keyboard([
			['І Пари Тудей', 'І Пари Тумороу', 'І Пари Тижня', 'І Пари Некст Тижня'],
			['Дзвінки', 'Хоме']
		])
			.oneTime(false)
			.resize()
			.extra({ parse_mode: 'Markdown' })
	);
});

bot.hears('КСМ (КІ)', async (ctx) => {
	const m = new Markup();
	await ctx.replyWithMarkdown('*λ* Вибирай 👇',
		m.keyboard([
			['К Пари Тудей', 'К Пари Тумороу', 'К Пари Тижня', 'К Пари Некст Тижня'],
			['Дзвінки', 'Хоме']
		])
			.oneTime(false)
			.resize()
			.extra({ parse_mode: 'Markdown' })
	);
});

bot.hears(/(І|К) Пари (Тудей|Тумороу)/, async (ctx) => {
	if (!ctx.match)
		return await ctx.replyWithMarkdown('*λ* Поломив -_-');
	const part = getPart();
	const day = moment().get('weekday') - (ctx.match[2] === 'Тудей' ? 2 : 1);
	if (day < 0 || day > 4)
		return await ctx.replyWithMarkdown('*λ* Поломив -_-');
	const s = schedule[ctx.match[1] === 'І' ? 'ІПЗ' : 'КСМ']
		.map((day) => day.map((item) => {
			if (Array.isArray(item))
				return item[part];
			else
				return item;
		}))
		[day]
		.map((item, i) => {
			let subject = `   *[${i + 1}]* `;
			if (item === null)
				subject += '-';
			else if (item.room)
				subject += `${item.name} (${item.room})`;
			else
				subject += item.name;
			return subject;
		});
	await ctx.replyWithMarkdown(`*=== ${week[day]} ===*\n${s.join('\n')}`);
});

bot.hears(/(І|К) Пари (Некст )?Тижня/, async (ctx) => {
	if (!ctx.match)
		return await ctx.replyWithMarkdown('*λ* Поломив -_-');
	const part = ctx.match[2] ? Number(!getPart()) : getPart();
	const s = schedule[ctx.match[1] === 'І' ? 'ІПЗ' : 'КСМ']
		.map((day) => day.map((item) => {
			if (Array.isArray(item))
				return item[part];
			else
				return item;
		}))
		.map((day) => day.map((item, i) => {
			let subject = `   *[${i + 1}]* `;
			if (item === null)
				subject += '-';
			else if (item.room)
				subject += `${item.name} (${item.room})`;
			else
				subject += item.name;
			return subject;
		}));
	await ctx.replyWithMarkdown(s.map((item, i) => `*=== ${week[i]} ===*\n${item.join('\n')}`).join('\n'));
});

bot.hears('Дзвінки', async (ctx) => {
	await ctx.replyWithMarkdown(c.map((item, i) => `*[${i + 1}]* ${item}`).join('\n'));
});

bot.hears('Хоме', async (ctx) => {
	const m = new Markup();
	await ctx.replyWithMarkdown('*λ* Кнопошкі 👇',
		m.keyboard([
			['ІПЗ', 'КСМ (КІ)']
		])
			.oneTime(false)
			.resize()
			.extra({ parse_mode: 'Markdown' })
	);
})

if (process.env.NODE_ENV === 'production') {
	bot.telegram.setWebhook('https://knit.dandrii.now.sh/' + process.env.TOKEN);
	createServer(bot.webhookCallback('/' + process.env.TOKEN)).listen();
} else
	bot.launch();
